# Onda 1 - 18h

## Windows update
```bash
ansible-playbook -i win-server-host.inv \
    --ask-vault-pass \
    --extra-vars '@../passwd-windows.yaml' \
    ../playbooks/win_update.yaml --check 

## AWS Update
```bash
ansible-playbook -i onda1-18.inv \
    --ask-vault-pass \
    --extra-vars '@../passwd.yaml' \
    ../playbooks/update-aws-ec2.yaml --check 

## Update basic

```bash
ansible-playbook -i onda1-18.inv ../playbooks/update-basic.yaml --check
```

## Jenkins basic

```bash
ansible-playbook -i onda1-18.inv ../playbooks/update-jenkins.yaml --check

## Snapshot

```bash
ansible-playbook -i onda1-18.inv \
    --ask-vault-pass \
    --extra-vars '@../passwd.yaml' \
    --extra-vars '@../my_vars.yaml' \
    ../playbooks/update-snapshot.yaml --check
```

## Postgres

```bash
ansible-playbook -i onda1-18.inv \
    --ask-vault-pass \
    --extra-vars '@../passwd.yaml' \
    --extra-vars '@../my_vars.yaml' \
    ../playbooks/update-postgres.yaml --check
```

## Mongo

```bash
ansible-playbook -i onda1-18.inv \
    --ask-vault-pass \
    --extra-vars '@../passwd.yaml' \
    --extra-vars '@../my_vars.yaml' \
    ../playbooks/update-mongo.yaml --check
```

## Reboot

```bash
ansible-playbook -i onda1-18.inv reboot.yaml --check
```

## Check

```bash
ansible -i onda1-18.inv  -m shell -a "uptime" basic
ansible -i onda1-18.inv  -m shell -a "uptime" snapshot
ansible -i onda1-18.inv  -m shell -a "uptime" mongo
ansible -i onda1-18.inv  -m shell -a "uptime" reboot
```
